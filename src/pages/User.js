import { useEffect } from "react";
import BreadCrumb from "../components/BreadCrumb";
import ProductFilter from "../components/content/products-list/ProductFilter";
import UserInfo from "../components/content/user/UserInfo";
import Footer from "../components/Fotter";
import Header from "../components/Header";

export default function User (){
    
    useEffect(() => {
        window.scrollTo(0, 0);
      }, []);
    return(
        <>
            <Header></Header>
            <BreadCrumb></BreadCrumb>
            <UserInfo></UserInfo>
            <Footer></Footer>
        </>
    )
}