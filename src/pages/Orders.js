import { useEffect } from "react";
import BreadCrumb from "../components/BreadCrumb";
import OrdersContent from "../components/content/orders/OrdersContent";
import ProductFilter from "../components/content/products-list/ProductFilter";
import UserInfo from "../components/content/user/UserInfo";
import Footer from "../components/Fotter";
import Header from "../components/Header";

export default function Orders (){
    
    useEffect(() => {
        window.scrollTo(0, 0);
      }, []);
    return(
        <>
            <Header></Header>
            <BreadCrumb></BreadCrumb>
            <OrdersContent></OrdersContent>
            <Footer></Footer>
        </>
    )
}