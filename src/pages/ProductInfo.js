import { Typography } from "@mui/material";
import { useState,useEffect } from "react";
import { useParams } from "react-router-dom";
import BreadCrumb from "../components/BreadCrumb";
import InfoContent from "../components/content/product-info/InfoContent";
import Footer from "../components/Fotter";
import Header from "../components/Header";
import { useDispatch, useSelector } from "react-redux";

export default function ProductInfo (){
    const {refresh} = useSelector((reduxData) => reduxData.changeLinkReducer)
    const [data,setData]= useState()
    let params = useParams();
    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }
    
    
    //console.log(typesId)
    useEffect(()=>{
        getData("http://localhost:8000/product/"+params.productId)
            .then((res) => {   
                setData(res.data)
            })
            .catch((error) => {
                console.log(error)
            })},[refresh,params.productId]
    )
    
    return(data?
        <>
            <Header></Header>
            <BreadCrumb > </BreadCrumb>
            <InfoContent id={params.productId} data={data}></InfoContent>   
            <Footer></Footer>
        </>:<></>
    )
}