import { useEffect } from "react";
import BreadCrumb from "../components/BreadCrumb";

import ProductFilter from "../components/content/products-list/ProductFilter";
import Footer from "../components/Fotter";
import Header from "../components/Header";

export default function ProductList (){
    
    useEffect(() => {
        window.scrollTo(0, 0);
      }, []);
    return(
        <>
            <Header></Header>
            <BreadCrumb></BreadCrumb>
            <ProductFilter></ProductFilter>
            <Footer></Footer>
        </>
    )
}