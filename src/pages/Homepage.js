
import { useEffect } from "react";
import BreadCrumb from "../components/BreadCrumb";
import HomepageContent from "../components/content/homePage/HomepageContent";
import Footer from "../components/Fotter";
import Header from "../components/Header";

export default function Homepage() {
    useEffect(() => {
        window.scrollTo(0, 0);
      }, []);
    return (
        <div >
            <Header></Header>
            <BreadCrumb></BreadCrumb>
            <HomepageContent></HomepageContent>
            <Footer></Footer>
        </div>
    )
}