import { Grid } from "@mui/material";
import { useEffect } from "react";
import BreadCrumb from "../components/BreadCrumb";
import ListProduct from "../components/content/cart/ListProduct";
import TotalPrice from "../components/content/cart/TotalPrice";
import Footer from "../components/Fotter";
import Header from "../components/Header";

export default function Cart (){
    
    useEffect(() => {
        window.scrollTo(0, 0);
      }, []);
    return(
        <>
            <Header></Header>
            <BreadCrumb></BreadCrumb>
            <Grid container>
                <Grid item xs={12} md={8}>
                    <ListProduct></ListProduct>
                </Grid>
                <Grid item xs={12} md={3.5}>
                    <TotalPrice></TotalPrice>
                </Grid>
            </Grid>
           
            <Footer></Footer>
        </>
    )
}