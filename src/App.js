import './App.css';
import Homepage from './pages/Homepage';
import { Route, Routes } from 'react-router-dom';
import ProductList from './pages/ProductList';
import ProductInfo from './pages/ProductInfo';
import Cart from './pages/Cart';
import '@coreui/coreui/dist/css/coreui.min.css'
import Login from './pages/Login';
import User from './pages/User';
import { useEffect } from 'react';
import Orders from './pages/Orders';



function App() {

  return (
    <div >
      
      <Routes>
        <Route path="/"  element={<Homepage></Homepage>}></Route>
        <Route path="/products" element={<ProductList></ProductList>}></Route>
        <Route path="/products/:productId" element={<ProductInfo/>} />
        <Route path="/cart" element={<Cart/>} />
        <Route path="/login" element={<Login/>} />
        <Route path="/user" element={<User/>} />
        <Route path="/orders" element={<Orders/>} />
      </Routes>
    </div>
  );
}

export default App;
