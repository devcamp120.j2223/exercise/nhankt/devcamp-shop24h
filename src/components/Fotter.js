import { Container, Grid, Typography } from "@mui/material";
import { Button } from "reactstrap";
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import TwitterIcon from '@mui/icons-material/Twitter';

export default function Footer() {
    const footerStyle= {
        marginBottom: "1rem",
    }
         return (
        <div style={{backgroundColor:"#A5C9CA"}}>
            <Container style={{ marginTop:"20px",  padding:"3rem 1rem 2rem 1rem"}}>
            <Grid container>
                <Grid  item  ml={3} xs={6} md={2.5} >
                    <Grid style={footerStyle} container><h2>PRODUCTS</h2></Grid>
                    <Grid style={footerStyle} container> Help center</Grid>
                    <Grid style={footerStyle} container> Contacg Us</Grid>
                    <Grid style={footerStyle} container> Product Help</Grid>
                    <Grid style={footerStyle} container> Warranty</Grid>
                    <Grid style={footerStyle} container> Order Status</Grid>
                </Grid>
                <Grid item ml={3} xs={4} md={2.5} mr={1}>
                    <Grid style={footerStyle} container><h2>SERVICES</h2></Grid>
                    <Grid style={footerStyle} container> Help center</Grid>
                    <Grid style={footerStyle} container> Contacg Us</Grid>
                    <Grid style={footerStyle} container> Product Help</Grid>
                    <Grid style={footerStyle} container> Warranty</Grid>
                    <Grid style={footerStyle} container> Order Status</Grid>
                </Grid>
                <Grid item ml={3} xs={6} md={2.5}>
                    <Grid style={footerStyle} container><h2>SUPPORT</h2></Grid>
                    <Grid style={footerStyle} container> Help center</Grid>
                    <Grid style={footerStyle} container> Contacg Us</Grid>
                    <Grid style={footerStyle} container> Product Help</Grid>
                    <Grid style={footerStyle} container> Warranty</Grid>
                    <Grid style={footerStyle} container> Order Status</Grid>
                </Grid>
                
               
                <Grid ml={3} item xs={4}  md={3}>
                <Grid container><h2>KT SHOP</h2></Grid>
                    <Grid container  >
                        <Grid item xs={6} md={2} ><Button href="#" style={{backgroundColor:"#A5C9CA",border:0}} ><FacebookIcon  htmlColor="#4267B2" /></Button></Grid>
                        <Grid item xs={6} md={2}><Button href="#" style={{backgroundColor:"#A5C9CA",border:0}}><InstagramIcon htmlColor="#C13584"/></Button></Grid>
                        <Grid item xs={6} md={2}><Button href="#" style={{backgroundColor:"#A5C9CA",border:0}}><YouTubeIcon htmlColor="#FF0000"/></Button></Grid>
                        <Grid item xs={6} md={2}><Button href="#" style={{backgroundColor:"#A5C9CA",border:0}}><TwitterIcon htmlColor="#1DA1F2"/></Button></Grid>
                    </Grid>
                </Grid>
            </Grid>
        </Container>
        </div>
        

    )
}