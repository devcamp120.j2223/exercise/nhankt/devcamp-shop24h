import { Breadcrumbs, Stack, Typography, Container } from "@mui/material";
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";


const data = [
    {
        "_id": "62d79be97e1a56359bb926eb",
        "name": "wf-1000xm4",
        "type": "62d7a4ee7e1a56359bb92730",
        "imageUrl": "https://bizweb.dktcdn.net/thumb/1024x1024/100/451/485/products/tai-nghe-khong-day-sony-wf-1000xm4-2-1.jpg?v=1652505152173",
        "buyPrice": 5000000,
        "promotionPrice": 3800000,
        "amount": 12,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0,
        "description": "tai nghe WF-1000XM4 kết hợp công nghệ chống ồn tiên tiến nhất, với hiệu suất âm thanh vượt trội và các tính năng thông minh có thể chỉnh riêng, thay đổi theo vị trí của bạn"
    },
    {
        "_id": "62d79c4e7e1a56359bb926ee",
        "name": "wh-1000xm4",
        "type": "62d7a4ee7e1a56359bb92730",
        "imageUrl": "https://www.sony.com.vn/image/5d02da5df552836db894cead8a68f5f3",
        "buyPrice": 6000000,
        "promotionPrice": 5200000,
        "amount": 83,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0,
        "description": "tai nghe Wh-1000XM4 kết hợp công nghệ chống ồn tiên tiến nhất, thời lượng pin cao, với hiệu suất âm thanh vượt trội và các tính năng thông minh có thể chỉnh riêng, thay đổi theo vị trí của bạn"
    },
    {
        "_id": "62d79ca27e1a56359bb926f1",
        "name": "airpod-3",
        "type": "62d7a4ee7e1a56359bb92730",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/a/i/airpod-3.jpg",
        "buyPrice": 4800000,
        "promotionPrice": 4500000,
        "amount": 152,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0,
        "description": "tai nghe airpod thời trang, phong cách sang trọng, gọn nhẹ"
    },
    {
        "_id": "62d79d427e1a56359bb926f4",
        "name": "Samsung Galaxy S22",
        "type": "62d7a4e97e1a56359bb9272e",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/s/m/sm-s901_galaxys22_front_pinkgold_211122.jpg",
        "buyPrice": 21800000,
        "promotionPrice": 18000000,
        "amount": 252,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0,
        "description": "Điện thoại mới nhất của nhà samsung"
    },
    {
        "_id": "62d79d817e1a56359bb926f7",
        "name": "iPhone 13 Pro Max 128GB",
        "type": "62d7a4e97e1a56359bb9272e",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/i/p/iphone_13-_pro-5_4.jpg",
        "buyPrice": 34800000,
        "promotionPrice": 27000000,
        "amount": 452,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d79dbc7e1a56359bb926fa",
        "name": "Xiaomi 11T Pro",
        "type": "62d7a4e97e1a56359bb9272e",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/x/i/xiaomi-mi-11t-pro-price-in-usa-500x500.jpg",
        "buyPrice": 15000000,
        "promotionPrice": 12000000,
        "amount": 222,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d79e3c7e1a56359bb926fd",
        "name": "Samsung Galaxy S22 Ultra",
        "type": "62d7a4e97e1a56359bb9272e",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/s/m/sm-s908_galaxys22ultra_front_burgundy_211119.jpg",
        "buyPrice": 31000000,
        "promotionPrice": 26000000,
        "amount": 72,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d79e977e1a56359bb92700",
        "name": "iPhone 12 64GB",
        "type": "62d7a4e97e1a56359bb9272e",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/i/p/iphone_12_blue.png",
        "buyPrice": 23000000,
        "promotionPrice": 16000000,
        "amount": 42,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d79f747e1a56359bb92703",
        "name": "OPPO Find X3 Pro 5G",
        "type": "62d7a4e97e1a56359bb9272e",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/o/p/oppo-find-x3-pro-5g-3_2.jpg",
        "buyPrice": 27000000,
        "promotionPrice": 19000000,
        "amount": 95,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d79fb97e1a56359bb92706",
        "name": "Samsung Galaxy Tab S8 Ultra",
        "type": "62d7a5077e1a56359bb92732",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/t/a/tab_s8_ultra.jpg",
        "buyPrice": 31000000,
        "promotionPrice": 30000000,
        "amount": 195,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d79ff17e1a56359bb92709",
        "name": "iPad 10.2 2021 WiFi 64GB",
        "type": "62d7a5077e1a56359bb92732",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/i/p/ipad-10-2-2021-5_2.jpg",
        "buyPrice": 11000000,
        "promotionPrice": 8900000,
        "amount": 25,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d7a0257e1a56359bb9270c",
        "name": "Xiaomi Pad 5",
        "type": "62d7a5077e1a56359bb92732",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/o/1/o1cn01ijop4f1slqk1fdzto_-2201438992231_1628774717.jpg",
        "buyPrice": 9000000,
        "promotionPrice": 8900000,
        "amount": 15,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d7a06b7e1a56359bb9270f",
        "name": "Samsung Galaxy Tab A7 Lite",
        "type": "62d7a5077e1a56359bb92732",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/s/a/samsung-galaxy-tab-a7-lite-gray-600x600.jpg",
        "buyPrice": 4300000,
        "promotionPrice": 4300000,
        "amount": 9,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d7a0e97e1a56359bb92712",
        "name": "Macbook Pro 14 inch 2021",
        "type": "62d7a4da7e1a56359bb9272c",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/m/a/macbook-pro-2021-007_1.jpg",
        "buyPrice": 48000000,
        "promotionPrice": 48000000,
        "amount": 19,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d7a14c7e1a56359bb92715",
        "name": "Asus Gaming Rog Strix G15",
        "type": "62d7a4da7e1a56359bb9272c",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/2/_/2_64_38.jpg",
        "buyPrice": 24000000,
        "promotionPrice": 18000000,
        "amount": 29,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d7a1d57e1a56359bb92718",
        "name": "Asus Gaming Rog Zenphyrus G14",
        "type": "62d7a4da7e1a56359bb9272c",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/1/2/12_5_28.jpg",
        "buyPrice": 30000000,
        "promotionPrice": 24000000,
        "amount": 19,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d7a2517e1a56359bb9271b",
        "name": "Chuột không dây Logitech MX Master 2S",
        "type": "62d7a4d17e1a56359bb9272a",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/c/h/chuot-khong-day-logitech-mx-master-2s.jpg",
        "buyPrice": 2500000,
        "promotionPrice": 1700000,
        "amount": 50,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d7a2bc7e1a56359bb9271e",
        "name": "Chuột không dây Logitech MX Anywhere 3",
        "type": "62d7a4d17e1a56359bb9272a",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/c/h/chuot-khong-day-logitech-mx-anywhere-3-8.png",
        "buyPrice": 2000000,
        "promotionPrice": 1600000,
        "amount": 43,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d7a3017e1a56359bb92721",
        "name": "Chuột Apple Magic Mouse 2 ",
        "type": "62d7a4d17e1a56359bb9272a",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/2/_/2_18_44_1_2.jpg",
        "buyPrice": 3000000,
        "promotionPrice": 2500000,
        "amount": 73,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    },
    {
        "_id": "62d7a34f7e1a56359bb92724",
        "name": "Chuột Gaming Logitech G403 Hero",
        "type": "62d7a4d17e1a56359bb9272a",
        "imageUrl": "https://cdn2.cellphones.com.vn/358x/media/catalog/product/c/h/chuot-choi-game-co-day-logitech-g403-hero.jpg",
        "buyPrice": 1200000,
        "promotionPrice": 940000,
        "amount": 33,
        "timeCreated": "2022-07-20T05:50:41.736Z",
        "timeUpdated": "2022-07-20T05:50:41.736Z",
        "__v": 0
    }
]



export default function BreadCrumb( ) {
    const pathnames = window.location.href.split("/").filter(x => x != "http:" && !x.includes("localhost") && x);
       
         //(paramId.length>20? data.filter(el=>el._id==paramId)[0].name: paramId)
    const breadcrumbs1 = pathnames.map((item,index) => 
    (index == pathnames.length-1?<Typography>{item.length>20? data.filter(el=>el._id==item)[0].name: item}</Typography>:<Link
    underline="hover"
    key={index}
    color="blue"
    to= {"/"+pathnames.slice(0,index+1).join("/")}
>
    {item.length>20? data.filter(el=>el._id==item)[0].name: item}
</Link> )
    
    )
       
    const breadcrumbs = [
        <Link underline="hover" key="1" color="blue" to="/" onClick={() => { }}>
            HOME
        </Link>,
        breadcrumbs1

    ];

    return (
        <Container style={{ paddingTop: "90px" ,paddingBottom: "30px" }}>
            <Stack spacing={2}>
                <Breadcrumbs
                    separator={<NavigateNextIcon fontSize="small" />}
                    aria-label="breadcrumb"
                >
                    {breadcrumbs}
                </Breadcrumbs>
            </Stack>
        </Container>
    );

}