import { Button, Container, Grid, Typography, MenuItem, Menu } from "@mui/material";
import "../App.css"
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { auth, googleProvider } from "../firebase";
import { fontSize, fontWeight } from "@mui/system";

export default function Header() {
    const { cart } = useSelector((reduxData) => reduxData.DataCartReducer)
    const [user, setUser] = useState(null)
    const [anchorEl, setAnchorEl] = useState(null);
    const [open, setOpen] = useState(false);

    const navigate = useNavigate()
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClickUser = (event) => {
        //console.log(event.currentTarget.localName)
        setAnchorEl(event.currentTarget);
        setOpen(true);
    };
    const handleLogout = () => {
        auth.signOut()
            .then(() => {
                navigate("/")
                setUser(null)

            })
            .catch((er) => {
                console.log(er)
            })
    }

    const handleClose = () => {
        setOpen(false);
        setAnchorEl(null);

    };

    const handleCartClick = () => {
        if (user) { navigate("/cart"); window.scrollTo(0, 0) }
        else { navigate("/login") }
    }
    useEffect(() => {
        auth.onAuthStateChanged((res) => {
            setUser(res)
        })
    }, [])

    return (
        <div style={{ position: "fixed", width: "100%", zIndex: 1, backgroundColor: "#CEE5D0" }}>
            <Container sx={{ margin: "0 auto" }}>
                <Grid container paddingBottom={2} >
                    <Grid item xs={4}>
                        <Typography onClick={() => { window.scrollTo(0, 0) }} component={Link} to="/" style={{ color: "#80558C", textDecoration: "none", fontSize: "2rem", fontFamily:"Roboto" }} >Kt Shop</Typography>
                    </Grid>
                    <Grid item xs={8} >
                        <Grid container justifyContent="flex-end"  >
                            <Button component={Link} to="/products" onClick={() => { window.scrollTo(0, 0) }} ><NotificationsNoneOutlinedIcon fontSize="large" /></Button>

                            {user ?
                                <div className="text-center">
                                    <Button
                                        id="basic-button"
                                        aria-controls={open ? 'basic-menu' : undefined}
                                        aria-haspopup="true"
                                        aria-expanded={open ? 'true' : undefined}
                                        onClick={handleClickUser}
                                    >
                                        <img src={user.photoURL} width={45} height={45} style={{ borderRadius: "50%" }} alt="avatar"></img>

                                    </Button>
                                    <Menu
                                        id="basic-menu"
                                        anchorEl={anchorEl}
                                        open={open}
                                        onClose={handleClose}
                                        MenuListProps={{
                                            'aria-labelledby': 'basic-button',
                                        }}

                                    >
                                        <MenuItem component={Link} to="/user">Thông tin cá nhân</MenuItem>
                                        <MenuItem component={Link} to="/orders">Đơn hàng</MenuItem>
                                        <MenuItem onClick={handleLogout}>Logout</MenuItem>
                                    </Menu>
                                </div>
                                : <Button component={Link} to="/login" >
                                    <PermIdentityIcon fontSize="large" />
                                </Button>}


                            <Button className="navigation" >
                                <ShoppingCartOutlinedIcon onClick={handleCartClick} className="btn-cart" fontSize="large" />
                                <div className="navigation-content">
                                    {cart != null && cart.length > 0 ?
                                        cart.map((el, index) =>
                                            <Grid key={index} container borderBottom={1} py={1} onClick={() => navigate("/products/" + el.data._id)} >
                                                <Grid item xs={4}><img height="70px" src={el.data.imageUrl} ></img></Grid>
                                                <Grid item xs={6} my="auto">{el.data.name}</Grid>
                                                <Grid item xs={2} my="auto">{el.quantity}</Grid>
                                            </Grid>
                                        )
                                        : <Typography>Giỏ hàng trống</Typography>}
                                </div>
                            </Button>


                            {cart != null && cart.length > 0 ? <div
                                style={{
                                    justifyContent: "right",
                                    position: 'absolute',
                                    backgroundColor: 'red',
                                    borderRadius: 100,
                                    width: "1.5rem",
                                    marginRight: 3,
                                    top: 1,
                                    textAlign: "center",
                                    justifyContent: 'center',

                                }}>
                                {cart.length}
                            </div> : null}

                        </Grid>
                    </Grid>
                </Grid>


            </Container>
        </div>

    )
}