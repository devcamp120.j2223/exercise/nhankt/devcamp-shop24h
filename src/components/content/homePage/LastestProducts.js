import { Grid, Typography, Container, Button, CardActionArea, CardMedia, CardContent, Card } from "@mui/material";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";


//const data1 = fetch("localhost:8000/product?lmit=8")

export default function LasttestProducts() {
    const [data, setData] = useState([])
    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }
    useEffect(() => {
        getData("http://localhost:8000/product?limit=8")
            .then((res) => {
                //console.log(res.data)
                setData(res.data)
            })
            .catch((error) => {
                console.log(error)
            })
    }, []
    )


    const [numberOfItem, setNumberOfItem] = useState(8)

    return (
        <Container style={{}}>
            <Grid container width="100%">
                <Grid item xs={12} md={12}>
                    <Typography variant="h3" align="center" mt={10} mb={8}>LASTEST PRODUCT</Typography>

                </Grid>

            </Grid>

            <Grid container width="100%"  >
                {data.map((product, index) => (

                    <Grid  key={index} item xs={12} sm={6} md={3} align="center" >
                        <Card sx={{ maxWidth: 250, height: 350, marginBottom: "6.50px", border: "none", boxShadow: "none" }} >
                            <CardActionArea component={Link} to={"/products/" + product._id}>
                                <CardMedia
                                    component="img"
                                    height="240"
                                    width="100"
                                    image={product.imageUrl}
                                    alt="green iguana"
                                />
                                <CardContent>
                                    <Typography gutterBottom variant="h6" height={50} component="div">
                                        {product.name}
                                    </Typography>
                                    <Typography variant="body2" color="text.secondary" >
                                        <span style={{ textDecoration: "line-through", marginRight: "7px" }}>{product.buyPrice.toLocaleString()}</span>
                                        <span>{product.promotionPrice.toLocaleString()}</span>
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Grid>


                ))}
                <Grid container justifyContent="center">
                    <Button variant="contained" style={{ backgroundColor: "black", marginTop: "20px" }} component={Link} to="/products">Show all</Button>
                </Grid>
            </Grid>


        </Container>

    )
}