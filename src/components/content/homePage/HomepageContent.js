import CarouselProduct from "./CarouselProduct";
import LasttestProducts from "./LastestProducts";

export default function HomepageContent (){
    return(
        <div >
            <CarouselProduct></CarouselProduct>
            <LasttestProducts></LasttestProducts>
        </div>
    )
}