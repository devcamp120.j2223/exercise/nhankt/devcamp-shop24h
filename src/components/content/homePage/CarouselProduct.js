
import imageWf1000xm4 from "../../../assets/images/wf-1000xm4.jpg"
import imageGalaxyS22 from "../../../assets/images/galaxy-s22.webp"
import imageMacbook14inch from "../../../assets/images/macbook14inch.jpeg"
import { useState } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Button, Container, Grid, Typography } from '@mui/material';

import Slider from "react-slick";
import { border } from "@mui/system";

const items = [
  {
    src: imageWf1000xm4,
    brand: 'Sony',
    name: 'Tai nghe WF-1000XM4',
    describe: "Tai nghe WF-1000XM4 kết hợp công nghệ chống ồn tiên tiến nhất .",
    _id: "62d79be97e1a56359bb926eb",
    key: 1
  },
  {
    src: imageGalaxyS22,
    brand: 'Samsung',
    name: 'Điện thoại galaxy s22',
    describe: "Hội tụ tinh hoa công nghệ nhiếp ảnh động, chụp siêu chi tiết không còn là thử thách",
    _id: "62d79e3c7e1a56359bb926fd",
    key: 2
  },
  {
    src: imageMacbook14inch,
    brand: 'Apple',
    name: 'Macbook pro 14inch',
    describe: "MacBook Pro 14 M1 Pro | 16-Core CPU | 32GB RAM | 512GB SSD mới mang đến hiệu năng cao ấn tượng cho người dùng ",
    _id: "62d7a0e97e1a56359bb92712",
    key: 3
  }
];
const settings = {
  dots: true,
  lazyLoad: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  initialSlide: 2,


};
export default function CarouselProduct() {
  return (
    <div style={{ backgroundColor: "#FCF8E8" }}>
      <Container >
        <Slider {...settings} >
          {items.map((item, index) => (
            <div key={index}  >
              <Grid container >
                <Grid item  xs={10.5} md={4} >
                  <Typography variant="h4" mt={1} >{item.brand}</Typography>
                  <Typography variant="h2" mt={3}>{item.name}</Typography>
                  <Typography variant="h6" mt={3} mb={4}>{item.describe}</Typography>
                  <Button variant="contained" href={`/products/${item._id}`} style={{ backgroundColor: "black" }}>Shop now</Button>
                </Grid>
                <Grid item  xs={10.5} md={8} style={{marginTop:"auto", marginBottom:"auto"}} >
                  <Grid container justifyContent="center" >
                    <img  src={item.src} style={{ maxWidth: "80%", maxHeight:"80%"}}></img>
                  </Grid>

                </Grid>
              </Grid>
            </div>

          ))}
        </Slider>

      </Container>
    </div>

  )
}

