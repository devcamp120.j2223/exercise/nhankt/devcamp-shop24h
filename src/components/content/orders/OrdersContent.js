
import { TabContext, TabList, TabPanel } from "@mui/lab";
import { Box, Container, Grid, Tab } from "@mui/material";
import { useState, useEffect } from "react";
import { auth, googleProvider } from "../../../firebase";




export default function OrdersContent() {

    const [value, setValue] = useState("1");
    const [userOrders, setUserOrders] = useState([]);
    const [allProducts, setAllProducts] = useState([]);
    const [allTypes, setAllTypes] = useState([]);
    const handleChange = (e, value) => {

        setValue(value);
    };
    const getData = async (url, body) => {
        const res = await fetch(url, body)
        const resData = await res.json()
        return resData
    }
    useEffect(() => {
        auth.onAuthStateChanged((res1) => {
            getData(`http://localhost:8000/customer/email/${res1.email}`)
                .then(res => {

                    if (res.data) {
                        getData(`http://localhost:8000/orders?id=${res.data.orders.join(",")}`)
                            .then(res2 => {
                                setUserOrders(res2.data)
                            })
                    }
                })
                .catch((error) => {
                })
            getData("http://localhost:8000/product")
                .then((res) => {
                    setAllProducts(res.data)
                })
                .catch((er) => {
                    console.log(er)
                })
            getData("http://localhost:8000/productType")
                .then((res) => {
                    setAllTypes(res.data)
                })
                .catch((er) => {
                    console.log(er)
                })
        })
    }, [])
    useEffect(() => {
        //console.log(userOrders)
    }, [userOrders])
    const Order = (paramArray) => {
        return (
            userOrders.length > 0 && allProducts.length > 0 && allTypes.length > 0 ?
                paramArray.map((el, index0) =>
                    <Container key={index0} style={{ marginBottom: "40px", backgroundColor: "#F7F6DC", padding: "1rem" }}>
                        <Grid justifyContent="flex-end" container>Tình trạng đơn hàng: <span style={{ fontWeight: "bold" }}>&nbsp;{el.orderStatus}</span> </Grid>
                        <Grid container>
                            <Grid item xs={12} mx="auto" borderBottom={0.5} my={1}></Grid>
                        </Grid>
                        {el.orderDetail.map((el1, index) =>
                            <Grid key={index} mb={3} container mt={index>0?7:2} maxHeight="110px">
                                <Grid item md={3} xs={3} >
                                    <Grid container justifyContent="center">
                                        <img style={{ maxHeight: "100px", maxWidth: "100%" }} src={allProducts.filter(el2 => el2._id == el1.productId)[0].imageUrl}></img>

                                    </Grid>
                                </Grid>
                                <Grid item md={9} xs={9} >
                                    <Grid container>Tên sản phẩm: {allProducts.filter(el2 => el2._id == el1.productId)[0].name} </Grid>
                                    <Grid container>Loại sản phẩm: {allTypes.filter(el => el._id == allProducts.filter(el2 => el2._id == el1.productId)[0].type)[0].name} </Grid>
                                    <Grid container justifyContent="flex-end" mt={2} borderBottom={0.5}>
                                            <Grid item md={6} xs={6} textAlign="end">
                                                Số lượng: {el1.quantity}
                                            </Grid>
                                            <Grid item md={6} xs={12} pb={3} >
                                                <Grid container justifyContent="flex-end">
                                                    Đơn giá:
                                                    <span style={{ color: "red", marginLeft: "0.6rem" }}>{allProducts.filter(el2 => el2._id == el1.productId)[0].promotionPrice.toLocaleString()}</span>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                </Grid>
                                
                            </Grid>
                        )}
                        <Grid container justifyContent="flex-end" >
                            <Grid item md={9}>
                                <Grid container mt={3}>
                                    <Grid item md={6} xs={12}>
                                        Ngày giao : {el.shippedDate.slice(0, 10)}
                                    </Grid>
                                    <Grid item md={6} xs={12} mt={3}>
                                        <Grid container justifyContent="flex-end">
                                            Tổng tiền: {el.cost.toLocaleString()}
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>


                        </Grid>


                    </Container>
                )
                : null
        )
    }
    return (
        <Container>
            <Box sx={{ width: '100%' }}>
                <TabContext value={value}>
                    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <TabList onChange={handleChange} aria-label="basic TabList example">
                            <Tab label="Tất cả đơn hàng" value="1" />
                            <Tab label="Chờ Xác nhận" value="2" />
                            <Tab label="Đã Xác nhận" value="3" />
                            <Tab label="Đang vận chuyển" value="4" />
                            <Tab label="Đã Hoàn thành" value="5" />
                            <Tab label="Đã huỷ" value="6" />
                        </TabList>
                    </Box>
                    <TabPanel value="1" >
                        {Order(userOrders)}
                    </TabPanel>
                    <TabPanel value="2">
                        {Order(userOrders.filter(el0 => el0.orderStatus == "open"))}
                    </TabPanel>
                    <TabPanel value="3" >
                        {Order(userOrders.filter(el0 => el0.orderStatus == "confirm"))}
                    </TabPanel>
                    <TabPanel value="4" >
                        {Order(userOrders.filter(el0 => el0.orderStatus == "shipping"))}
                    </TabPanel>
                    <TabPanel value="5" >
                        {Order(userOrders.filter(el0 => el0.orderStatus == "done"))}
                    </TabPanel>
                    <TabPanel value="6" >
                        {Order(userOrders.filter(el0 => el0.orderStatus == "cancel"))}
                    </TabPanel>
                </TabContext>

            </Box>
        </Container>

    );
}
