
import { Button, Grid, Typography , Snackbar, Alert} from "@mui/material";
import { Container } from "@mui/system";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
export default function Detail({ id, data }) {

    const dispatch = useDispatch()
    //const {cart} = useSelector((reduxData) => reduxData.DataCartReducer)
    const [quantity, setQuantity] = useState(1)
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")
    const productSeclected = data

    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const addProduct = () => {
        let cart = JSON.parse(localStorage.getItem("cart"))
        let newProduct = { data: data, quantity: quantity, isCheck: true }
        if (cart === null) {
            cart = [newProduct]
            console.log(cart)
        }
        else {
            cart = cart.filter(el => el.data._id != newProduct.data._id)
            cart.push(newProduct)
        }
        setOpenAlert(true);
        setStatusModal("success")
        setNoidungAlertValid("đã thêm vào giỏ hàng")
        dispatch({
            type: "PRODUCTS_OF_CART_HANDLER",
            payload: {
                cart: cart
            }
        });
        /* localStorage.setItem("cart",JSON.stringify(cart))
        console.log(JSON.parse(localStorage.getItem("cart")) ) */
    }
    useEffect(() => {
        setQuantity(1)
    }, [data])
    return (
        <Container>
            <Grid container>
                <Grid item md={5} xs={6} mt={7}>
                    <img src={productSeclected.imageUrl} width="100%"></img>
                </Grid>
                <Grid item md={7} xs={6}>
                    <Typography variant="h3" mb={3} mt={5}>{productSeclected.name}</Typography>
                    <Typography variant="h6" mb={3}>{productSeclected.description}</Typography>
                    <Typography variant="h6" color="text.secondary" mb={3} >
                        Price : <span style={{ textDecoration: "line-through", marginRight: "7px" }}>{productSeclected.buyPrice.toLocaleString()}</span>
                        <span style={{ color: "red" }}>{productSeclected.promotionPrice.toLocaleString()}</span>
                    </Typography>
                    <Typography>
                        <Button onClick={() => setQuantity(quantity - 1)} ><RemoveCircleIcon style={{ color: "black" }} /></Button>
                        {quantity < 1 ? 1 : quantity}
                        <Button onClick={() => setQuantity(quantity + 1)}><AddCircleIcon style={{ color: "black" }} /></Button>
                    </Typography>
                    <Grid container mt={2} ml={2}>
                        <Button style={{ background: "black", color: "white" }} onClick={addProduct} >ADD TO CART</Button>
                    </Grid>
                </Grid>
            </Grid>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </Container>
    )
}