import { Container, Grid, Typography ,Button} from "@mui/material";
import { useState } from "react";


export default function Description({id, data}) {
    const productSeclected = data
    const [divHeight,setDivHeight]= useState("700px")
    return (
        <Container >
            <div style={{ height: divHeight,position:"relative", overflow:"hidden" }}>
                <Typography variant="h3" mb={3}> Description </Typography>
                <Typography fontSize="1.5rem">{productSeclected.description + productSeclected.description + productSeclected.description}</Typography>
                <Grid item xs={12} align="center" >
                    <Grid container >
                    <img src={productSeclected.imageUrl} width="80%"  ></img>
                    </Grid>
                </Grid>
            </div>
            <Grid item xs={12} align="center" mt={3} position="relative">
                <Button style={{background:"black", color:"white"}} onClick={()=>{setDivHeight("")}}>VIEW ALL</Button>
            </Grid>

        </Container>
    )
}