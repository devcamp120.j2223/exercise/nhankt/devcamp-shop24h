import { Typography,Card,Grid,CardActionArea,CardMedia,CardContent,Container } from "@mui/material";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";



export default function Related ({id, data}){
    const {refresh} = useSelector((reduxData) => reduxData.changeLinkReducer)
    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }
    
    const [relatdProducts,setRelatedProducts] = useState([])
    const dispatch = useDispatch()
    const handleChangeLink = ()=>{
        dispatch({
            type: "LINK_OF_PRODUCT_HANDLER",
            payload: {
                refresh: !refresh
            }
          });
    }
    //console.log(typesId)
    useEffect(()=>{
        getData("http://localhost:8000/product?type="+data.type)
            .then((res) => {   
                setRelatedProducts(res.data.filter(el=>el._id!=id))
            })
            .catch((error) => {
                console.log(error)
            })},[refresh]
    )

    useEffect(() => {
        window.scrollTo(0, 0)
      }, [window.location.href])
    return(
        <Container style={{marginLeft:"30px"}}>
            <Typography variant="h3" mt={5}>Related Products</Typography>
            <Grid container  mt={5}>
                        {relatdProducts.map((product, index) => (
                            <Grid key={index} item md={4} xs={8} align="center" >
                                <Card sx={{ width: 280, height: 400, marginBottom: "60px", border: "none", boxShadow: "none" }} >
                                    <CardActionArea component={Link} to={"/products/"+product._id} onClick={handleChangeLink}>
                                        <CardMedia
                                            component="img"
                                            height="280"
                                            width="200"
                                            image={product.imageUrl}
                                            alt="green iguana"
                                        />
                                        <CardContent>
                                            <Typography gutterBottom variant="h6" height={50} component="div">
                                                {product.name}
                                            </Typography>
                                            <Typography variant="body2" color="text.secondary" >
                                                <span style={{ textDecoration: "line-through", marginRight: "7px" }}>{product.buyPrice.toLocaleString()}</span>
                                                    <span>{product.promotionPrice.toLocaleString()}</span>
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                </Card>
                            </Grid>))}
                    </Grid>
        </Container>
    )
}