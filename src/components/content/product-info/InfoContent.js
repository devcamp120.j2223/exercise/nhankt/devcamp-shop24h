import Description from "./Description";
import Detail from "./Detail";
import Related from "./Related";



export default function InfoContent ( {id, data} ){
    return(
        <>
            <Detail id={id} data={data}></Detail>
            <Description id={id} data={data}></Description>
            <Related id={id} data={data}></Related>
        </>
    )
}