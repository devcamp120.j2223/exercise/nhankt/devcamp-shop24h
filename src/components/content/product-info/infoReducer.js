const initialState = {
    refresh: true
  };
export default function changeLinkReducer (state = initialState, action){
    switch (action.type) {
        
        case "LINK_OF_PRODUCT_HANDLER" : {
            return {
                ...state,
                refresh: action.payload.refresh
              };
        }

        default: {
            return state;
          }
    }
}