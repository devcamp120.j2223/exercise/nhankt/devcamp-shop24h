const initialState = {
    minPrice: "",
    maxPrice: "",
    currentPage: 1,
    name : "",
    typesId: [
        
    ]
  };
export default function filterReducer (state = initialState, action){
    switch (action.type) {
        case "MIN_PRICE_HANDLER" : {
            return {
                ...state,
                minPrice: action.payload.minPrice
              };
        }
        case "MAX_PRICE_HANDLER" : {
            return {
                ...state,
                maxPrice: action.payload.maxPrice
              };
        }
        case "CURRENT_PAGE_HANDLER" : {
            return {
                ...state,
                currentPage: action.payload.currentPage
              };
        }
        case "NAME_HANDLER" : {
            return {
                ...state,
                name: action.payload.name
              };
        }
        case "TYPES_ID_HANDLER" : {
            if (!action.payload.isCheck) {
                var newTypesId = state.typesId.filter(el=>el!=action.payload.typesId)
                return {
                    ...state,
                    typesId: newTypesId
                  };
            }
            else {
                var newTypesId1 = state.typesId
                newTypesId1.push(action.payload.typesId)
                return {
                    ...state,
                    typesId: newTypesId1
                };
            }
          
        }
        default: {
            return state;
          }
    }
}