

import { FormControlLabel, FormGroup, Grid, Input, Checkbox, Container, Typography, Card, CardActionArea, CardMedia, CardContent, Pagination, Button } from "@mui/material";
import { useState, useEffect } from "react";
import NumberFormat from "react-number-format";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";


export default function ProductFilter() {
    const dispatch = useDispatch()
    const { minPrice, maxPrice, currentPage, name, typesId } = useSelector((reduxData) => reduxData.filterReducer)
    const [data, setData] = useState([])
    const [allProducts, setAllproducts]= useState(0)
    const [types, setTypes] = useState([])
    const [link, setLink] = useState("http://localhost:8000/product?limit=9")
    const [numberOfPages, setNumberOfPages] = useState(5)

    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }

    //console.log(typesId)
    useEffect(() => {
        getData("http://localhost:8000/productType")
            .then((res) => {
                setTypes(res.data)
            })
            .catch((er) => {
                console.log(er)
            })
        getData("http://localhost:8000/product")
            .then((res) => {
                setNumberOfPages(Math.ceil(res.data.length / 9))
            })
            .catch((er) => {
                console.log(er)
            })
    }, [])
    useEffect(() => {
        getData(link + `&skip=${(currentPage - 1) * 9}`)
            .then((res) => {
                setData(res.data)
            })
            .catch((error) => {
                console.log(error)
            })
    }, [currentPage, link]
    )
    const minPriceHandle = event => {
        const { formattedValue, value } = event;
        dispatch({
            type: "MIN_PRICE_HANDLER",
            payload: {
                minPrice: value
            }
        });
    }
    const maxPriceHandle = event => {
        console.log(event)
        const { formattedValue, value } = event;
        dispatch({
            type: "MAX_PRICE_HANDLER",
            payload: {
                maxPrice: value
            }
        });
    }

    const currentPageHandle = (event, value) => {
        dispatch({
            type: "CURRENT_PAGE_HANDLER",
            payload: {
                currentPage: value
            }
        });
    }
    const nameHandle = event => {
        dispatch({
            type: "NAME_HANDLER",
            payload: {
                name: event.target.value
            }
        });
    }
    const typesIdHandle = (isCheck, typeID) => {
        dispatch({
            type: "TYPES_ID_HANDLER",
            payload: {
                isCheck: isCheck,
                typesId: typeID
            }
        });


    }

    const handleFilterQuery = () => {
        dispatch({
            type: "CURRENT_PAGE_HANDLER",
            payload: {
                currentPage: 1
            }
        });
        let query = "http://localhost:8000/product?limit=9";
        if (minPrice != "") { query += "&min=" + minPrice }
        if (maxPrice != "") { query += "&max=" + maxPrice }
        if (name != "") { query += "&name=" + name }
        if (typesId.length < 5 && typesId.length > 0) { query += "&type=" + typesId.join(",") }
        console.log(query)
        setLink(query)
    }
    return (
        <Container style={{}}>
            <Grid container >
                <Grid item xs={12} md={2.5} >
                    <Grid container><Typography variant="h5" mb={1}>Tên </Typography> </Grid>
                    <Grid container mb={7}>
                        <Input onChange={nameHandle} style={{ width: "80%" }}>{name}</Input>
                    </Grid>
                    <Grid container><Typography variant="h5" mb={2}>Giá</Typography> </Grid>
                    <Grid container mb={2}>
                        <Typography mt={0.8} mr={1}>Min</Typography>
                        <NumberFormat thousandSeparator={true} style={{ width: "60%", fontSize: "1.1rem" }} onValueChange={minPriceHandle} value={minPrice}></NumberFormat>
                    </Grid>
                    <Grid container mb={6}>
                        <Typography mt={0.8} mr={0.5}>Max</Typography>
                        <NumberFormat thousandSeparator={true} style={{ width: "60%", fontSize: "1.1rem" }} onValueChange={maxPriceHandle} value={maxPrice}></NumberFormat>
                    </Grid>
                    <Grid ><Typography variant="h5" mb={2}>Loại</Typography> </Grid>
                    <FormGroup>
                        {types.map((type, index) =>
                            <FormControlLabel key={index} control={<Checkbox />} onChange={(event, value) => typesIdHandle(value, type._id)} label={type.name} />
                        )}

                    </FormGroup>
                    <Grid container mt={3} >
                        <Button style={{ background: "black", color: "white" }} onClick={handleFilterQuery} >FILTER</Button>
                    </Grid>
                </Grid>
                
                <Grid item xs={12} md={9.5} >
                    <Grid container width="100%" >
                        {data.map((product, index) => (
                            <Grid key={index} item xs={12} md={4} align="center" >
                                <Card sx={{ width: 280, height: 400, marginBottom: "60px", border: "none", boxShadow: "none" }} >
                                    <CardActionArea component={Link} to={"/products/" + product._id}>
                                        <CardMedia
                                            component="img"
                                            height="280"
                                            width="200"
                                            image={product.imageUrl}
                                            alt="green iguana"
                                        />
                                        <CardContent>
                                            <Typography gutterBottom variant="h6" height={50} component="div">
                                                {product.name}
                                            </Typography>
                                            <Typography variant="body2" color="text.secondary" >
                                                <span style={{ textDecoration: "line-through", marginRight: "7px" }}>{product.buyPrice.toLocaleString()}</span>
                                                <span>{product.promotionPrice.toLocaleString()}</span>
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                </Card>
                            </Grid>))}
                    </Grid>
                </Grid>
            </Grid>
            <Grid container mt={3} mb={2} justifyContent={"flex-end"}>
                <Grid item>
                    <Pagination count={numberOfPages} variant="outlined" onChange={currentPageHandle} page={currentPage} />
                </Grid>
            </Grid>
            
        </Container>
    )
}