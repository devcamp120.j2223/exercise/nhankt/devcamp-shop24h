import { Box,Button, TextField, MenuItem, Typography, Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Pagination, Snackbar, Alert } from "@mui/material"
import { Container } from "@mui/material";
import { useEffect, useState } from "react"
import { auth, googleProvider } from "../../../firebase";
import {Link, Routes, Route, useNavigate} from 'react-router-dom';


const style = {
    top: '50%',
    left: '50%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 3.5,
    maxHeight: '90%',

};


export default function UserInfo() {
    const citysArray = require(`../../../assets/locations/cities.json`).data


    const [user, setUser] = useState({ fullName: "", email: "", phone: "" })
    const [selectedCity, setSelectedCity] = useState("")
    const [selectedDistrict, setSelectedDistrict] = useState("")
    const [selectedWard, setSelectedWard] = useState("")
    const [street, setStreet] = useState("")
    const [districtsArray, setDistrictsArray] = useState([])
    const [wardsArray, setWardsArray] = useState([])
    const [initCustomerAddress,setInitCustomerAddress]= useState({city:"",district:"",ward:"",street:"",state:0})
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")
    const [refreshPage, setRefreshPage] = useState(Boolean(true))

    const customer = {
        fullName: user.fullName,
        phone: user.phone,
        email: user.email,
        address: {
            city: citysArray.filter(el => el.id == selectedCity)[0] ? citysArray.filter(el => el.id == selectedCity)[0].name : "",
            district: districtsArray.filter(el => el.id == selectedDistrict)[0] ? districtsArray.filter(el => el.id == selectedDistrict)[0].name : "",
            ward: wardsArray.filter(el => el.id == selectedWard)[0] ? wardsArray.filter(el => el.id == selectedWard)[0].name : "",
            street: street
        },
    }
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }
    useEffect(() => {
        auth.onAuthStateChanged((res1) => {
            setUser({ ...user, email: res1.email })
            getData(`http://localhost:8000/customer/email/${res1.email}`)
            .then(res=>{
                
                if (res.data) {
                    setUser({fullName: res.data.fullName , phone: res.data.phone, email: res.data.email})
                    setSelectedCity(citysArray.filter(el => el.name == res.data.address.city)[0].id)
                    setStreet(res.data.address.street)
                    setInitCustomerAddress({...initCustomerAddress, ...res.data.address})
                    
                }
            })
            .catch((error) => {
                
                setOpenAlert(true);
                setStatusModal("warning")
                setNoidungAlertValid("Chưa cập nhật địa chỉ")
            })
        })  
    }, [])
    useEffect(()=>{
        if (selectedDistrict=="" && districtsArray.length>0&& initCustomerAddress.district!="" && initCustomerAddress.state<2) {
            
            console.log(initCustomerAddress)
            setSelectedDistrict(districtsArray.filter(el => el.name == initCustomerAddress.district)[0].id)
            setInitCustomerAddress({...initCustomerAddress, state :initCustomerAddress.state+1})

        }
    },[districtsArray,initCustomerAddress])
    useEffect(()=>{
        if (selectedWard=="" &&wardsArray.length>0 && initCustomerAddress.ward!=""&& initCustomerAddress.state<2){
            setSelectedWard(wardsArray.filter(el => el.name == initCustomerAddress.ward)[0].id)
            setInitCustomerAddress({...initCustomerAddress, state :initCustomerAddress.state+1})

        }

    },[wardsArray,initCustomerAddress])
    useEffect(() => {
        if (selectedCity != "") {

            let newDistrictArray = require(`../../../assets/locations/districts/${selectedCity}.json`).data
            setDistrictsArray(newDistrictArray)
            setWardsArray([])
            //console.log(newDistrictArray)
        }
    },[selectedCity])
    useEffect(() => { 
        if (selectedDistrict != "") {
          
            let newWardArray = require(`../../../assets/locations/wards/${selectedDistrict}.json`).data
            setWardsArray(newWardArray)
            //console.log(newWardArray)
        }
    },[selectedDistrict])
    
    const navigate = useNavigate();
    const handleChangeSelectedCity = (event) => {
        setSelectedCity(event.target.value)
        
    }
    const handleChangeSelectedDistrict = (event) => {
        setSelectedDistrict(event.target.value)
        setSelectedWard("")
    }
    const handleChangeSelectedWard = (event) => {
        setSelectedWard(event.target.value)
    }
    const handleStreet = (event) => {
        setStreet(event.target.value)
    }
    const handleConfirm = (e) => {
        e.preventDefault();
        
        const body = {
            method: 'PUT',
            body: JSON.stringify(customer),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        }
        getData(`http://localhost:8000/customer/email/${customer.email}`, body)
            .then((data) => {
                if (data.message=="số điện thoại đã được đăng ký"){
                    setOpenAlert(true);
                    setStatusModal("error")
                    setNoidungAlertValid("số điện thoại đã được đăng ký")
                }  
                else {
                    setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã cập nhật")
                setRefreshPage(!refreshPage)
                } 
                
            })
            .catch((error) => {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("Dữ liệu thêm thất bại!")
            })

    }
    return (

        <Container>
            <Box onSubmit={handleConfirm} component="form" sx={style}>
                <Typography variant="h5" mb={1}>Cập nhật thông tin</Typography>
                <Grid container >
                    <Grid item xs={12} md={12} mb={3.2} mx="auto">
                        <TextField
                            required
                            fullWidth
                            size="small"
                            label="Họ và tên"
                            value={user.fullName }
                            onChange={(event) => { setUser({ ...user, fullName: event.target.value }) }}
                        />
                    </Grid>
                    <Grid item xs={12} md={12} mb={3.2} mx="auto">
                        <TextField
                            required
                            fullWidth
                            size="small"
                            label="Số điện thoại"
                            value={user.phone}
                            onChange={(event) => { setUser({ ...user, phone: event.target.value }) }}
                        />
                    </Grid>
                </Grid>
                <Grid container >
                    <Grid item xs={12} md={12} mb={3.3} mx="auto">
                        <TextField
                            inputProps={
                                { readOnly: true, }
                            }
                            fullWidth
                            size="small"
                            label="Email"
                            value={user.email}
                        />
                    </Grid>
                </Grid>

                <Grid container  >
                    <Grid item xs={12} md={12} mb={3.5} mx="auto">
                        <TextField
                            fullWidth
                            size="small"
                            label="Tỉnh/Thành Phố"
                            select
                            value={selectedCity}
                            onChange={handleChangeSelectedCity}
                        >
                            {citysArray.map((el, index) => <MenuItem key={index} value={el.id}>{el.name}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={12} md={12} mb={3.5} mx="auto">
                        <TextField
                            select
                            fullWidth
                            size="small"
                            label="Quận/Huyện"
                            value={selectedDistrict}
                            onChange={handleChangeSelectedDistrict}
                        >
                            {districtsArray.map((el, index) => <MenuItem key={index} value={el.id}>{el.name}</MenuItem>)}
                        </TextField>
                    </Grid>
                </Grid>
                <Grid container >
                    <Grid item xs={12} md={12} mb={3.5} mx="auto">
                        <TextField
                            select
                            fullWidth
                            size="small"
                            label="Phường/Xã"
                            value={selectedWard}
                            onChange={handleChangeSelectedWard}
                        >
                            {wardsArray.map((el, index) => <MenuItem key={index} value={el.id}>{el.name}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={12} md={12} mb={3.5} mx="auto" >
                        <TextField
                            fullWidth
                            size="small"
                            label="Đường, số nhà"
                            value={street}
                            onChange={handleStreet}
                        />
                    </Grid>
                </Grid>

                <Grid item xs={12} md={6} mx="auto" width="50%" borderBottom={0.5} my={3.5}></Grid>

                <Grid display="flex" justifyContent="right">
                    <Button variant="contained" color="primary" onClick={() => navigate(-1)} style={{marginRight:"10px"}} >Quay lại</Button>

                    <Button variant="contained" color="success" type="submit" >Xác nhận</Button>
                </Grid>
            </Box>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </Container>
    )
}