
const initialState = {
  cart : JSON.parse(localStorage.getItem("cart")),
  displayOrderModal : false
};
export default function DataCartReducer (state = initialState, action){
    switch (action.type) {
        case "PRODUCTS_OF_CART_HANDLER" : {
          localStorage.setItem("cart",JSON.stringify(action.payload.cart))
            return {
                ...state,
                cart: action.payload.cart
              };
        }
        case "DISPLAY_MODAL_HANDLER" : {
            return {
                ...state,
                displayOrderModal: action.payload.displayOrderModal
              };
        }

        default: {
            return state;
          }
    }
}