import { Button, Container, Grid, Typography, Checkbox, Box } from "@mui/material"
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import { useDispatch, useSelector } from "react-redux";
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { CModal,CModalHeader,CModalTitle, CModalFooter,CButton,CModalBody} from "@coreui/react";
import { useEffect, useState } from "react";


export default function ListProduct() {
    let productsOfCart = JSON.parse(localStorage.getItem("cart"))
    const [modal, setModal] = useState({visible:false,index:''})
    const { cart } = useSelector((reduxData) => reduxData.DataCartReducer)
    const dispatch = useDispatch()
    const handleDecrease = (paramIndex) => {
        let newcart = cart.map((el, index) => index == paramIndex ? { data: el.data, quantity: el.quantity > 1 ? el.quantity - 1 : 1, isCheck:el.isCheck } : el)
        dispatch({
            type: "PRODUCTS_OF_CART_HANDLER",
            payload: {
                cart: newcart
            }
        });
    }
    const handleIncrease = (paramIndex) => {
        let newcart = cart.map((el, index) => index == paramIndex ? { data: el.data, quantity: el.quantity + 1, isCheck:el.isCheck  } : el)
        dispatch({
            type: "PRODUCTS_OF_CART_HANDLER",
            payload: {
                cart: newcart
            }
        });
    }
    const handleDelete = (paramIndex) => {
        let newcart = cart.filter((el, index) => index != paramIndex)
        dispatch({
            type: "PRODUCTS_OF_CART_HANDLER",
            payload: {
                cart: newcart
            }
        });
        setModal({visible:false,index:""})
    }
    const handleCheck = (paramIndex)=>{
        let newcart = cart.map((el, index) => index == paramIndex ? { data: el.data, quantity: el.quantity , isCheck:!el.isCheck } : el)
        dispatch({
            type: "PRODUCTS_OF_CART_HANDLER",
            payload: {
                cart: newcart
            }
        });
    }
    return (

        <Container>
            <Grid>
                    <CModal visible={modal.visible} onClose={() => setModal({visible:false,index:""})}>
                        <CModalHeader onClose={() => setModal({visible:false,index:""})}>
                            <CModalTitle></CModalTitle>
                        </CModalHeader>
                        <CModalBody>Bạn có chắc chắn muốn xoá sản phẩm này ra khỏi giỏ hàng</CModalBody>
                        <CModalFooter>
                            <CButton color="secondary" onClick={() => setModal({visible:false,index:""})}>
                                Close
                            </CButton>
                            <CButton color="primary" onClick={()=>handleDelete(modal.index)}>Save changes</CButton>
                        </CModalFooter>
                    </CModal>
                </Grid>
            <Grid container>
                <Grid item xs={12} mr={4}>
                    {/* <Grid container mb={5} height="3rem"  textAlign="center" style={{ backgroundColor: "#f5f5f5" }}>
                        
                        <Grid item md={3} xs={6} my="auto" >Ảnh</Grid>
                        <Grid item md={2} xs={6} my="auto">Tên sản phẩm</Grid>
                        <Grid item md={2} xs={6} my="auto">Đơn giá</Grid>
                        <Grid item md={2} xs={6} my="auto">Số lượng</Grid>
                        <Grid item md={2}  xs={6} my="auto">Thành tiền</Grid>
                    </Grid> */}

                    <Grid container>
                        {cart.map((el, index) =>
                            <Grid key={index} container mb={3} textAlign="center" borderBottom={0.1} borderRight={0.1} >
                                <Grid item md={0.5} xs={2} my="auto"><Box><Checkbox onChange={()=>handleCheck(index)} checked={el.isCheck}/></Box></Grid>
                                <Grid item md={2.5} xs={5} my="auto"><img src={el.data.imageUrl} style={{ height: "8rem" }}></img></Grid>
                                <Grid item md={2}  xs={5}my="auto" >{el.data.name}</Grid>
                                <Grid item md={2}  xs={6}my="auto">{el.data.promotionPrice.toLocaleString()}</Grid>

                                <Grid item md={2}  xs={6}my="auto">
                                    <Grid container ml={1}>
                                        <Button onClick={() => handleDecrease(index)}><RemoveCircleIcon style={{ color: "black" }} /></Button>
                                        <Typography>{el.quantity}</Typography>
                                        <Button onClick={() => handleIncrease(index)}><AddCircleIcon style={{ color: "black" }} /></Button>
                                    </Grid>
                                </Grid>
                                <Grid item md={2} xs={6}my="auto">{(el.quantity * el.data.promotionPrice).toLocaleString()}</Grid>
                                <Grid item md={1}  xs={6}my="auto">
                                    <Button  size="large" startIcon={<DeleteForeverIcon onClick={() => setModal({visible:true,index:index})} />}></Button>
                                    
                                    </Grid>
                            </Grid>
                        )}
                    </Grid>
                </Grid>
                
            </Grid>

        </Container>
    )
}