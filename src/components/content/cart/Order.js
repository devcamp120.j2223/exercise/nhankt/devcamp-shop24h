import { Button, Snackbar, Alert, Grid, Radio, Typography, TextField, Modal, Box, Select, MenuItem, InputLabel, RadioGroup, FormControlLabel, Icon, FormControl } from "@mui/material";
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import { auth, googleProvider } from "../../../firebase";
import {Link, Routes, Route, useNavigate} from 'react-router-dom';

import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width:"90%",
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 3.5,
    maxHeight:'90%',
    overflow:'scroll',
};
const DAY_AFTER_CONFIRMED_ORDER = 5;
const SHIPPING_FEE = 20000
const citysArray = require(`../../../assets/locations/cities.json`).data
export default function Order({ subToltal }) {

    const dispatch = useDispatch()
    const [user, setUser] = useState({ fullName: "", email: "", phone: "" })

    const { displayOrderModal, cart } = useSelector((reduxData) => reduxData.DataCartReducer)
    const [selectedCity, setSelectedCity] = useState("")
    const [selectedDistrict, setSelectedDistrict] = useState("")
    const [selectedWard, setSelectedWard] = useState("")
    const [street, setStreet] = useState("")
    const [districtsArray, setDistrictsArray] = useState([])
    const [wardsArray, setWardsArray] = useState([])
    const [note, setNote] = useState("")

    const [initCustomerAddress,setInitCustomerAddress]= useState({city:"",district:"",ward:"",street:"",state:0})
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")
    const [refreshPage, setRefreshPage] = useState(Boolean(true))

    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    const getData = async (paramUrl, body) => {
        const response = await fetch(paramUrl, body);
        const responseData = await response.json();
        return responseData;
    }
    useEffect(() => {
        auth.onAuthStateChanged((res1) => {
            getData(`http://localhost:8000/customer/email/${res1.email}`)
            .then(res=>{ 
                setUser({...user, email: res1.email})
                if (res.data) {
                    setUser({...user,email: res1.email,fullName: res.data.fullName?res.data.fullName:"" , phone: res.data.phone?res.data.phone:""}) 
                    setSelectedCity(res.data.address?citysArray.filter(el => el.name == res.data.address.city)[0].id:"")
                    setStreet(res.data.address?res.data.address.street:"")
                    setInitCustomerAddress({...initCustomerAddress, ...res.data.address})
                }
            })
            /* .catch((error) => {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("Load dữ liệu thất bại")
            }) */
        })  
    }, [])
    useEffect(()=>{
        if (selectedDistrict=="" && districtsArray.length>0&& initCustomerAddress.district!="" && initCustomerAddress.state<2) {
            //console.log(initCustomerAddress)
            setSelectedDistrict(districtsArray.filter(el => el.name == initCustomerAddress.district)[0].id)
            setInitCustomerAddress({...initCustomerAddress, state :initCustomerAddress.state+1})

        }
    },[districtsArray,initCustomerAddress])
    useEffect(()=>{
        if (selectedWard=="" &&wardsArray.length>0 && initCustomerAddress.ward!=""&& initCustomerAddress.state<2){
            setSelectedWard(wardsArray.filter(el => el.name == initCustomerAddress.ward)[0].id)
            setInitCustomerAddress({...initCustomerAddress, state :initCustomerAddress.state+1})

        }

    },[wardsArray,initCustomerAddress])
    useEffect(() => {
        if (selectedCity != "") {

            let newDistrictArray = require(`../../../assets/locations/districts/${selectedCity}.json`).data
            setDistrictsArray(newDistrictArray)
            setWardsArray([])
            //console.log(newDistrictArray)
        }
    },[selectedCity])
    useEffect(() => { 
        if (selectedDistrict != "") {
          
            let newWardArray = require(`../../../assets/locations/wards/${selectedDistrict}.json`).data
            setWardsArray(newWardArray)
            //console.log(newWardArray)
        }
    },[selectedDistrict])
    
    const navigate = useNavigate();
    const handleChangeSelectedCity = (event) => {
        setSelectedCity(event.target.value)
        
    }
    const handleChangeSelectedDistrict = (event) => {
        setSelectedDistrict(event.target.value)
        setSelectedWard("")
    }
    const handleChangeSelectedWard = (event) => {
        setSelectedWard(event.target.value)
    }
    const handleStreet = (event) => {
        setStreet(event.target.value)
    }
   
    const handleConfirm = (e) => {
        e.preventDefault();
        ///cần bổ sung valid dữ liệu
        let shippedDate = new Date();
        shippedDate.setDate(shippedDate.getDate() + DAY_AFTER_CONFIRMED_ORDER);
        const customer = {
            fullName: user.fullName,
            phone: user.phone,
            email: user.email,
        }
        const order = {
            shippedDate: shippedDate,
            orderDetail: cart.filter(el => el.isCheck).map(el=> ({productId:el.data._id, quantity: el.quantity, name: el.data.name})),
            shippingAddress: {
                city: citysArray.filter(el => el.id == selectedCity)[0].name,
                district: districtsArray.filter(el => el.id == selectedDistrict)[0].name,
                ward: wardsArray.filter(el => el.id == selectedWard)[0].name,
                street: street
            },
            cost: SHIPPING_FEE + subToltal,
            note: note
        }
        const data = { customer: customer, order: order }

        

        const body = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            }
        }
        getData("http://localhost:8000/orders/",body)
        .then((data) => {
            if (data.message=="số điện thoại đã được đăng ký"){
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("số điện thoại đã được đăng ký")
            }  
            else {
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("đã cập nhật")
                setRefreshPage(!refreshPage)
                dispatch({
                    type: "DISPLAY_MODAL_HANDLER",
                    payload: {
                        displayOrderModal: false
                    }
                })
                let vCart = JSON.parse(localStorage.getItem("cart"))
                let newCart = vCart.filter(el=>!el.isCheck)
                dispatch({
                    type: "PRODUCTS_OF_CART_HANDLER",
                    payload: {
                        cart: newCart
                    }
                })           
            } 
            
        })
        .catch((error) => {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Dữ liệu thêm thất bại!")
        })

    }
    
    useEffect(()=>{},[displayOrderModal])


    return (
        <>
        <Modal
            open={displayOrderModal}
            onClose={() => {
                dispatch({
                    type: "DISPLAY_MODAL_HANDLER",
                    payload: {
                        displayOrderModal: false
                    }
                })
            }}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Box onSubmit={handleConfirm} component="form" sx={style}>
                <Typography variant="h5" mb={1}>THÔNG TIN ĐƠN HÀNG</Typography>
                <Grid container >
                    <Grid item xs={12} md={5.5} mb={3.2} mx="auto">
                        <TextField
                            required
                            fullWidth
                            size="small"
                            label="Họ và tên"
                            value={user.fullName }
                            onChange={(event) => { setUser({ ...user, fullName: event.target.value }) }}
                        />
                    </Grid>
                    <Grid item xs={12} md={5.5} mb={3.2} mx="auto">
                        <TextField
                            required
                            fullWidth
                            size="small"
                            label="Số điện thoại"
                            value={user.phone}
                            onChange={(event) => { setUser({ ...user, phone: event.target.value }) }}
                        />
                    </Grid>
                </Grid>
                <Grid container >
                    <Grid item xs={12} md={11.5} mb={3.3} mx="auto">
                        <TextField
                            required
                            fullWidth
                            inputProps={
                                { readOnly: true, }
                            }
                            size="small"
                            label="Email"
                            value={user.email}
                        />
                    </Grid>
                </Grid>
                <Grid container  mb={3.5}> Địa chỉ giao hàng </Grid>  
                <Grid container  >
                    <Grid item xs={12} md={5.5} mb={3.5} mx="auto">
                        <TextField
                            required
                            fullWidth
                            size="small"
                            label="Tỉnh/Thành Phố"
                            select
                            value={selectedCity}
                            onChange={handleChangeSelectedCity}
                        >
                            {citysArray.map((el, index) => <MenuItem key={index} value={el.id}>{el.name}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={12} md={5.5} mb={3.5} mx="auto">
                        <TextField
                            required
                            select
                            fullWidth
                            size="small"
                            label="Quận/Huyện"
                            value={selectedDistrict}
                            onChange={handleChangeSelectedDistrict}
                        >
                            {districtsArray.map((el, index) => <MenuItem key={index} value={el.id}>{el.name}</MenuItem>)}
                        </TextField>
                    </Grid>
                </Grid>
                <Grid container >
                    <Grid item xs={12} md={5.5} mb={3.5} mx="auto">
                        <TextField
                            required
                            select
                            fullWidth
                            size="small"
                            label="Phường/Xã"
                            value={selectedWard}
                            onChange={handleChangeSelectedWard}
                        >
                            {wardsArray.map((el, index) => <MenuItem key={index} value={el.id}>{el.name}</MenuItem>)}
                        </TextField>
                    </Grid>
                    <Grid item xs={12} md={5.5} mb={3.5} mx="auto" >
                        <TextField
                            required
                            fullWidth
                            size="small"
                            label="Đường, số nhà"
                            value={street}
                            onChange={handleStreet}
                        />
                    </Grid>
                </Grid>
                <Grid container >
                    <Grid item xs={12} md={11.5} mx="auto">
                        <TextField
                            multiline
                            fullWidth
                            size="large"
                            label="Ghi chú thêm"
                            value={note}
                            onChange={(event) => { setNote(event.target.value) }}
                        />
                    </Grid>
                </Grid>
                <Grid item xs={12} md={6} mx="auto" borderBottom={0.5} my={4}></Grid>

                <Grid container >
                    <Grid item xs={12} md={5.5} mx="auto">
                        <Grid container><Typography fontWeight="800">Thành tiền</Typography></Grid>
                        <Grid container my={1.8}>
                            <Grid item xs={6} md={6} >
                                <Typography>Tiền sản phẩm :</Typography>
                            </Grid>
                            <Grid item xs={6} md={5} display="flex" justifyContent="right">
                                <Typography >{subToltal.toLocaleString()} VND</Typography>
                            </Grid>
                        </Grid>
                        <Grid container my={1.8}>
                            <Grid item xs={6} md={6} >
                                <Typography>Phí ship :</Typography>
                            </Grid>
                            <Grid item xs={6} md={5} display="flex" justifyContent="right">
                                <Typography >{(SHIPPING_FEE).toLocaleString()} VND</Typography>
                            </Grid>
                        </Grid>
                        <Grid container my={1.8}>
                            <Grid item xs={6} md={6} my="auto"><TextField
                                size="small"
                                label="Voucher giảm giá"
                                onChange={() => { }}
                            />
                            </Grid>
                            <Grid item xs={6} md={5} display="flex" justifyContent="right">
                                <Typography my="auto" >{(0).toLocaleString()} VND</Typography>
                            </Grid>
                        </Grid>
                        <Grid container my={1.5}>
                            <Grid item xs={6} md={5}  >
                                <Typography >Tổng cộng :</Typography>
                            </Grid>
                            <Grid item xs={6} md={6} display="flex" justifyContent="right">
                                <Typography >{(subToltal + SHIPPING_FEE).toLocaleString()} VND</Typography>
                            </Grid>
                        </Grid>

                    </Grid>
                    <Grid item xs={12} md={5.5} mx="auto">
                        <Grid container mb={1} >
                            <Grid item > <Typography fontWeight="800">Phương thức thanh toán</Typography></Grid>
                        </Grid>
                        <Grid container >
                            <Grid item xs={12} md={12} mx="auto" >
                                <RadioGroup onChange={() => { }} required defaultValue="COD" >
                                    <FormControlLabel label="COD (thanh toán khi nhận)" value="COD" control={<Radio />} />
                                    <FormControlLabel label="Thẻ Visa/Master Card" value="CARD" control={<Radio />} />
                                    <FormControlLabel label="Ví điện tử MOMO" value="MOMO" control={<Radio />} />
                                </RadioGroup>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>


                <Grid item xs={12} md={6} mx="auto" borderBottom={0.5} my={3.5}></Grid>

                <Grid display="flex" justifyContent="right">
                    <Button variant="contained" color="success" type="submit" >Xác nhận</Button>
                </Grid>
                
            </Box>
        </Modal>
        <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert} anchorOrigin={{ vertical: 'top', horizontal: 'center' }}>
        <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
            {noidungAlertValid}
        </Alert>
    </Snackbar>
    </>
    )
}