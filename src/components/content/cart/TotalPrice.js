import { Button, Grid, Input, Typography, TextField, Modal, Box } from "@mui/material";
import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Order from "./Order";

export default function TotalPrice() {
    const { cart } = useSelector((reduxData) => reduxData.DataCartReducer)
    const dispatch = useDispatch()
    var subToltal = 0;
    cart.forEach(element => {
        if (element.isCheck) {
            subToltal += element.quantity * element.data.promotionPrice
        }
    });

    return (
        <>
            <Grid container mb={2} height="3rem" textAlign="center" style={{ backgroundColor: "#f5f5f5" }}>
                <Grid item xs={12} my="auto">Thành tiền</Grid>
            </Grid>
            <Grid container height={80} >
                <Grid item xs={3.5} my="auto">Tổng cộng</Grid>
                <Grid item xs={8.5} my="auto"><Typography>{subToltal.toLocaleString()}</Typography></Grid>
            </Grid>
            <Grid item xs={6} mx="auto" borderBottom={0.5} my={4}></Grid>
            <Grid container textAlign="end" >
                <Grid item xs={12} mt={3}>
                    <Button variant="contained" onClick={() => dispatch({
                        type: "DISPLAY_MODAL_HANDLER",
                        payload: {
                            displayOrderModal: true
                        }
                    })}>Tạo đơn hàng</Button>
                </Grid>
            </Grid>
            <Order subToltal={subToltal}></Order>
        </>

    )
}