import firebase from "firebase";

import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyDMLDHksXcbts6nHJTqgiML3-ubGWMMSh8",
    authDomain: "devcamp-shop24h-19c0b.firebaseapp.com",
    projectId: "devcamp-shop24h-19c0b",
    storageBucket: "devcamp-shop24h-19c0b.appspot.com",
    messagingSenderId: "833717987390",
    appId: "1:833717987390:web:41720a357e9d3ffff6496f"
  };


  firebase.initializeApp(firebaseConfig);

  export const auth = firebase.auth();

  export const googleProvider = new firebase.auth.GoogleAuthProvider();