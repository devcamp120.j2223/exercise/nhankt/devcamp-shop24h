import { createStore, combineReducers } from "redux";

import filterReducer from "../components/content/products-list/filterReducer";
import changeLinkReducer from "../components/content/product-info/infoReducer";
import DataCartReducer from "../components/content/cart/DataCartReducer";
const appReducer = combineReducers({ 
    filterReducer: filterReducer,
    changeLinkReducer : changeLinkReducer,
    DataCartReducer: DataCartReducer
});

const store = createStore(
    appReducer,
    {},
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;